Portfolio

Hi , and Welcome to my Portfolio!
My name is Moe Darwish, and I am relative new in the IT industry.
I started in 2022 and study since then in Coders.Bay Vienna as an app developer.

Projects

**Java Projects**


[Data-Management](https://gitlab.com/codersMoe/abschlussfinal)


 My first project was made in a team of two, where we constructed a website for the administrator at Coders.Bay. The purpose was to be able to manage data from participants and trainers, as well as group assignments. Additionally, we implemented an overview of the groups, where the trainers and participants are shown.


[Plastic Slug](https://gitlab.com/codersbay-fia-3/hello-world-2d-game)

Recently, our team of 9 has been working on the creation of a 2D Game. This marks our first slightly larger group, aimed to gain valuable experience. The game itself will be a simple single-player / multiplayer game, involving mouse aiming and keyboard movement, with obstacles and enemies to overcome. The goal of the game is to complete stages, defeat the boss, and survive by skillfully dodging enemy projectiles.


**Kotlin Project**

[Note App](https://gitlab.com/codersMoe/note)

This app serves as a powerful tool for note-taking enthusiasts. Seamlessly create, organize, and manage your notes. Stay productive and organized with ease. Keep an eye out for future updates, including achievement tracking, to witness your progress as a note-taking maestro.
